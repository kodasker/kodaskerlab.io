---
title: Hexo ile Gitlab üzerinde blog oluşturma
date: 2017-07-21 21:20:01
tags:
- Hexo
- Git
- Gitlab
---

Kurulum
------------------

>  - [Git](https://git-scm.com/)
>  - [Node.js](https://nodejs.org/en/)

Hexo kurulumu ve proje oluşturma
-------------
Kurulumları yaptıktan sonra terminal de sırasıyla aşağıdaki komutları çalıştırıyoruz.
```
npm install hexo-cli -g

```
<!-- more -->

![](/images/hexo-cli.gif)

```
hexo init kodasker.gitlab.io

cd kodasker.gitlab.io

npm install
```


![](/images/hexo-init.gif)

Bu işlemlerin ardından Gitlab'a  giriş yaparak projemizi oluşturuyoruz.

![](/images/gitlab-new-project.png)

![](/images/gitlab-project-name.png)

Git kurulumu ve ayarları
------------
Tekrar terminalimizi açıp aşağıda ki komutlarımızı çalıştırıyoruz.

```
git config --global user.name "Kodasker" 

git config --global user.email "kodasker@gmail.com"

git init

git remote add origin https://gitlab.com/kodasker/kodasker.gitlab.io.git

```
![](/images/git-setup.gif)


Git ile projemizi yayınlıyoruz
------------------------

```
git add . 

git commit -m "ilk kurulum"

git push -u origin master
```

![](/images/git-push.gif)


Son adım
--------
Projemizi Gitlab hesabımıza başarıyla push ettik. Şimdi projemizin çalışabilmesi için repomuzda  .gitlab-ci.yml dosyası oluşturuyoruz.

![](/images/gitlab-new-file.png)
![](/images/gitlab-new-ci.png)

```
image: node:4.2.2

pages:
  cache:
    paths:
    - node_modules/

  script:
  - npm install hexo-cli -g
  - npm install
  - hexo deploy
  artifacts:
    paths:
    - public
  only:
  - master

```

Eklediğimiz CI ın başarıyla tamamlanmasını bekliyoruz. Job tamamlandıktan sonra blogumuzun yayın da olduğunu görebiliriz. 
![](/images/gitlab-pipelines.png)
![](/images/gitlab-pipelines-running.png)
![](/images/gitlab-pipelines-job-done.png)

Ve blogumuz yayında.
--------------------
 Blog adresimiz : [http://kodasker.gitlab.io/](http://kodasker.gitlab.io/)

![](/images/gitlab-pages-published.png)

